import 'package:e_commerce_sport_shirts/Cards/CartObject.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

class CartCard extends StatelessWidget {
  CartCard({
    Key key,
    @required this.imagePath,
    @required this.pieceName,
    @required this.tradeMark,
    @required this.costPrice,
  }) : super(key: key);
  Money costPrice;
  String imagePath;
  String pieceName;
  String tradeMark;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      width: MediaQuery.of(context).size.width,
      color: Color.fromRGBO(255, 255, 255, 1),
      padding: EdgeInsets.fromLTRB(
          MediaQuery.of(context).size.width * 0.1,
          MediaQuery.of(context).size.height * 0.01,
          0.0,
          MediaQuery.of(context).size.height * 0.0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            CartObject(
                imagePath: imagePath,
                pieceName: pieceName,
                tradeMark: tradeMark,
                costPrice: costPrice),
            CartObject(
                imagePath: imagePath,
                pieceName: pieceName,
                tradeMark: tradeMark,
                costPrice: costPrice),
          ],
        ),
      ),
    );
  }
}
