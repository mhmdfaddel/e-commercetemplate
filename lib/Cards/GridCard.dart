import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

class GridCard extends StatelessWidget {
  GridCard({
    Key key,
    @required this.costPrice,
    @required this.imagePath,
    @required this.pieceName,
    @required this.tradeMark,
  }) : super(key: key);

  Money costPrice;
  String tradeMark;
  String pieceName;
  String imagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      width: 60.0,

      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.width * 0.5,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              image: DecorationImage(
                image: AssetImage('$imagePath'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.06,
                    MediaQuery.of(context).size.height * 0.02,
                    0,
                    MediaQuery.of(context).size.height * 0.01),
                child: GestureDetector(
                  onTap: () {},
                  child: Text(
                    '$pieceName',
                    style: TextStyle(),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.06,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.01),
                  child: GestureDetector(
                    onTap: () {},
                    child: Text(
                      '$tradeMark',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  )),
              Container(
                height: MediaQuery.of(context).size.height * 0.065,
                child: Row(
                  children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.06,
                            MediaQuery.of(context).size.height * 0.01,
                            0,
                            MediaQuery.of(context).size.height * 0.01),
                        child: GestureDetector(
                          onTap: () {},
                          child: Text('$costPrice'),
                        )),
                    Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.06,
                            MediaQuery.of(context).size.height * 0.01,
                            0,
                            MediaQuery.of(context).size.height * 0.01),
                        child: GestureDetector(
                          onTap: () {},
                          child: Text(
                            'ADD TO CART',
                            style: TextStyle(
                              color: Colors.blue,
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

//
