import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

class CartObject extends StatelessWidget {
  const CartObject({
    Key key,
    @required this.imagePath,
    @required this.pieceName,
    @required this.tradeMark,
    @required this.costPrice,
  }) : super(key: key);

  final String imagePath;
  final String pieceName;
  final String tradeMark;
  final Money costPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.08,
                width: MediaQuery.of(context).size.height * 0.1,
                padding: EdgeInsets.fromLTRB(
                    0.0,
                    MediaQuery.of(context).size.height * 0.02,
                    0.0,
                    MediaQuery.of(context).size.height * 0.1),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  image: DecorationImage(
                    image: AssetImage('$imagePath'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.00,
                        MediaQuery.of(context).size.height * 0.025,
                        MediaQuery.of(context).size.width * 0.0,
                        MediaQuery.of(context).size.height * 0.0),
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.09,
                    child: Text(
                      'Qty',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.0,
                MediaQuery.of(context).size.height * 0.006,
                MediaQuery.of(context).size.width * 0.03,
                MediaQuery.of(context).size.height * 0.0),
            child: Column(
              children: [
                Text(
                  '$pieceName',
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.03,
                      MediaQuery.of(context).size.height * 0.0,
                      MediaQuery.of(context).size.width * 0.25,
                      MediaQuery.of(context).size.height * 0.000),
                  child: Text(
                    '$tradeMark',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.0,
                      MediaQuery.of(context).size.height * 0.014,
                      MediaQuery.of(context).size.width * 0.00,
                      MediaQuery.of(context).size.height * 0.000),
                  child: Row(
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Colors.black87,
                          primary: Colors.grey[300],
                          minimumSize: Size(36, 36),
                          alignment: Alignment(0, -1.00),
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(2)),
                          ),
                        ),
                        child: Icon(
                          Icons.minimize_outlined,
                          color: Colors.grey[600],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: Text(
                          '1',
                          style: TextStyle(
                            color: Colors.black45,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Colors.black87,
                          primary: Colors.grey[300],
                          minimumSize: Size(36, 36),
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(2)),
                          ),
                        ),
                        child: Icon(
                          Icons.add,
                          color: Colors.grey[600],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.1,
                    0, //MediaQuery.of(context).size.height*0.00012,
                    0.0,
                    MediaQuery.of(context).size.height * 0.07),
                child: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.delete,
                    color: Colors.grey[600],
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.02,
                      0, //MediaQuery.of(context).size.height*0.00012,
                      0.0,
                      MediaQuery.of(context).size.height * 0.001),
                  child: GestureDetector(
                    onTap: () {},
                    child: Text('$costPrice'),
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
