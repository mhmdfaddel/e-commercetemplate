import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

class HorCard extends StatelessWidget {
  HorCard({
    Key key,
    @required this.costPrice,
    @required this.imagePath,
    @required this.pieceName,
    @required this.tradeMark,
  }) : super(key: key);

  Money costPrice;
  String tradeMark;
  String pieceName;
  String imagePath;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 120.0,
          width: 120.0,
          margin: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            image: DecorationImage(
              image: AssetImage('$imagePath'),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(
                  0.0, 0, 0, MediaQuery.of(context).size.height * 0.01),
              child: GestureDetector(
                onTap: () {},
                child: Text(
                  '$pieceName',
                  style: TextStyle(),
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(
                    0.0, 0, 0, MediaQuery.of(context).size.height * 0.02),
                child: GestureDetector(
                  onTap: () {},
                  child: Text(
                    '$tradeMark',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                )),
            Container(
              height: MediaQuery.of(context).size.height * 0.065,
              child: Row(
                children: [
                  Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text('$costPrice'),
                      )),
                  Padding(
                      padding: EdgeInsets.only(left: 40.0),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          'ADD TO CART',
                          style: TextStyle(
                            color: Colors.blue,
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
