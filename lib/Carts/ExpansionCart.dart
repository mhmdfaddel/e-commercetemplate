import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/Cards/CartCard.dart';
import 'package:e_commerce_sport_shirts/customExpansionTile.dart' as custom;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

class ExpandedCart extends StatelessWidget {
  const ExpandedCart({
    Key key,
    @required this.imagePath,
    @required this.pieceName,
    @required this.tradeMark,
    @required this.costPrice,
  }) : super(key: key);

  final String imagePath;
  final String pieceName;
  final String tradeMark;
  final Money costPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.68,
      width: MediaQuery.of(context).size.width * 0.9,
      padding: EdgeInsets.fromLTRB(
          0.0,
          MediaQuery.of(context).size.height * 0.04,
          MediaQuery.of(context).size.width * 0.0,
          0),
      child: custom.ExpansionTile(
        headerBackgroundColor:
            Color.fromRGBO(202, 255, 235, 0.40784313725490196),
        title: Text(
          'Yes',
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.blue,
        children: <Widget>[
          CartCard(
              imagePath: imagePath,
              pieceName: pieceName,
              tradeMark: tradeMark,
              costPrice: costPrice),
          Container(
            padding: EdgeInsets.fromLTRB(
                0.0, 0, MediaQuery.of(context).size.width * 0.7, 0),
            child: Text(
              'Discount',
              style: TextStyle(color: Colors.grey),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.06,
            margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  color: Colors.grey, // set border color
                  width: 0.5,
                  style: BorderStyle.solid), // set border width
              // set rounded corner radius
            ),
            child: Row(
              children: [
                TextField(
                  decoration: InputDecoration(
                    hintText: '',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    border: InputBorder.none,
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.black87,
                    primary: Colors.grey[300],
                    minimumSize: Size(36, 36),
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                  ),
                  child: Text('Apply'),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Row(
                children: [
                  Container(
                    child: Text('Subtotal'),
                    margin: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.05,
                      MediaQuery.of(context).size.height * 0.02,
                      MediaQuery.of(context).size.width * 0.57,
                      MediaQuery.of(context).size.height * 0.01,
                    ),
                  ),
                  Container(child: Text(' ')),
                ],
              ),
              Row(
                children: [
                  Container(
                    child: Text('Shipping'),
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.05,
                        MediaQuery.of(context).size.height * 0.01,
                        MediaQuery.of(context).size.width * 0.57,
                        0.0 // MediaQuery.of(context).size.height*0.06,
                        ),
                  ),
                  Container(child: Text(' ')),
                ],
              ),
              Row(
                children: [
                  Container(
                    child: Text('Taxes'),
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.05,
                        MediaQuery.of(context).size.height * 0.02,
                        MediaQuery.of(context).size.width * 0.57,
                        0.0 // MediaQuery.of(context).size.height*0.06,
                        ),
                  ),
                  Container(child: Text('')),
                ],
              ),
              Row(
                children: [
                  Container(
                    child: Text('Total'),
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.05,
                        MediaQuery.of(context).size.height * 0.02,
                        MediaQuery.of(context).size.width * 0.57,
                        0.0 // MediaQuery.of(context).size.height*0.06,
                        ),
                  ),
                  Container(child: Text('')),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
