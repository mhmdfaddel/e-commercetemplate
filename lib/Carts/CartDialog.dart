import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/Cards/CartCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

class CartDialog extends StatelessWidget {
  CartDialog({
    Key key,
    @required this.imagePath,
    @required this.pieceName,
    @required this.tradeMark,
    @required this.costPrice,
  }) : super(key: key);
  Money costPrice;
  String imagePath;
  String pieceName;
  String tradeMark;

  @override
  Widget build(BuildContext context) {
    Money costPrice = Money.fromInt(165, Currency.create('USD', 0));
    String tradeMark = 'NIKE';
    String pieceName = 'BRASIL AUTHENTIC\nJERSEY 2018';
    String imagePath = 'assets/images/01.png';
    return Container(
      width: MediaQuery.of(context).size.width * 0.95,
      child: Dialog(
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.all(10),
          child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.8,
                  padding: EdgeInsets.fromLTRB(10, 50, 10, 20),
                  child: CartCard(
                    costPrice: costPrice,
                    imagePath: imagePath,
                    pieceName: pieceName,
                    tradeMark: tradeMark,
                  )),
            ],
          )),
    );
  }
}
