import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

class ReviewCard extends StatelessWidget {
  ReviewCard({
    Key key,
    @required this.imagePath,
    @required this.pieceName,
    @required this.tradeMark,
    @required this.costPrice,
  }) : super(key: key);
  Money costPrice;
  String imagePath;
  String pieceName;
  String tradeMark;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      width: MediaQuery.of(context).size.width * 0.9,
      color: Color.fromRGBO(255, 255, 255, 1),
      padding: EdgeInsets.fromLTRB(
          MediaQuery.of(context).size.width * 0.02,
          MediaQuery.of(context).size.height * 0.01,
          0.0,
          MediaQuery.of(context).size.height * 0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.09,
                width: MediaQuery.of(context).size.height * 0.09,
                padding: EdgeInsets.fromLTRB(
                    0.0,
                    MediaQuery.of(context).size.height * 0.02,
                    0.0,
                    MediaQuery.of(context).size.height * 0.1),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  image: DecorationImage(
                    image: AssetImage('$imagePath'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.0,
                MediaQuery.of(context).size.height * 0.006,
                MediaQuery.of(context).size.width * 0.03,
                MediaQuery.of(context).size.height * 0.0),
            child: Column(
              children: [
                Text(
                  '$pieceName',
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.03,
                      MediaQuery.of(context).size.height * 0.02,
                      MediaQuery.of(context).size.width * 0.25,
                      MediaQuery.of(context).size.height * 0.000),
                  child: Text(
                    '$tradeMark',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.03,
                          MediaQuery.of(context).size.height * 0.01,
                          MediaQuery.of(context).size.width * 0.01,
                          MediaQuery.of(context).size.height * 0.000),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          'Qty: 1 ',
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.03,
                          MediaQuery.of(context).size.height * 0.01,
                          MediaQuery.of(context).size.width * 0.08,
                          MediaQuery.of(context).size.height * 0.000),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          '$costPrice',
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.18,
                    0, //MediaQuery.of(context).size.height*0.00012,
                    0.0,
                    MediaQuery.of(context).size.height * 0.07),
                child: GestureDetector(
                  onTap: () {},
                  child: Text(
                    'EDIT',
                    style: TextStyle(
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
