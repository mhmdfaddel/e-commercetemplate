import 'package:e_commerce_sport_shirts/pages/CartPage.dart';
import 'package:e_commerce_sport_shirts/pages/Checkout_reviewPage.dart';
import 'package:e_commerce_sport_shirts/pages/Loading.dart';
import 'package:e_commerce_sport_shirts/pages/PaymentPage.dart';
import 'package:e_commerce_sport_shirts/pages/ProductDetails.dart';
import 'package:e_commerce_sport_shirts/pages/ProductSearch.dart';
import 'package:e_commerce_sport_shirts/pages/ShippingPage.dart';
import 'package:e_commerce_sport_shirts/pages/ThankYou.dart';
import 'package:e_commerce_sport_shirts/pages/explorer.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      //home: Home(),
      initialRoute: '/home',
      routes: {
        '/': (context) => Loading(),
        '/home': (context) => Explorer(),
        '/Payment': (context) => PaymentPage(),
        '/ProductSearch': (context) => ProductSearch(),
        '/ProductDetails': (context) => ProductDetails(),
        '/Thanks': (context) => ThanksPage(),
        '/ShippingReview': (context) => ShippingPage(),
        '/CartPage': (context) => CartPage(),
        '/CheckoutReview': (context) => CheckoutReview(),
      },
    ));
