import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/Cards/CartCard.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/MyDrawer.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/copyRights.dart';
import 'package:e_commerce_sport_shirts/customExpansionTile.dart' as custom;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/accessories/Cards/myAppBar.dart';

void main() => runApp(MaterialApp(
      home: ShippingPage(),
    ));

class ShippingPage extends StatefulWidget {
  @override
  _ShippingPage createState() => _ShippingPage();
}

enum ShippingMethod { free, cheap, expensive }

class _ShippingPage extends State<ShippingPage> {
  final appTitle = 'FIFA World Cup Kits';
  Money costPrice = Money.fromInt(165, Currency.create('USD', 0));
  String tradeMark = 'NIKE';
  String pieceName = 'BRASIL AUTHENTIC\nJERSEY 2018';
  String imagePath = 'assets/images/01.png';
  int _currentStep = 1;
  StepperType stepperType = StepperType.horizontal;
  var heigg = 0.12;
  ShippingMethod _method = ShippingMethod.free;
  bool visible = true;
  Money taxes = Money.from(11.5, Currency.create('USD', 0));
  Money total = Money.from(176.5, Currency.create('USD', 0));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar(
        title: this.appTitle,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0,
                MediaQuery.of(context).size.height * 0.02,
                MediaQuery.of(context).size.width * 0.58,
                MediaQuery.of(context).size.height * 0.01,
              ),
              child: Text(
                'Checkout',
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 30.0,
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.641,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 40.0,
                        height: 40.0,
                        padding: EdgeInsets.all(
                            MediaQuery.of(context).size.width * 0.03),
                        decoration: new BoxDecoration(
                          color: Colors.lightBlue,
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          '1',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                        width: 70.0,
                        child: Container(
                          color: Colors.grey,
                        ),
                      ),
                      Row(
                        children: [
                          Container(
                            width: 40.0,
                            height: 40.0,
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.width * 0.03),
                            decoration: new BoxDecoration(
                              color: Colors.grey,
                              shape: BoxShape.circle,
                            ),
                            child: Text(
                              '2',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          SizedBox(
                            height: 1.0,
                            width: 70.0,
                            child: Container(
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 40.0,
                            height: 40.0,
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.width * 0.03),
                            decoration: new BoxDecoration(
                              color: Colors.grey,
                              shape: BoxShape.circle,
                            ),
                            child: Text(
                              '3',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.08,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.0,
                          ),
                          child: Text('Shipping'),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.17,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.0,
                          ),
                          child: Text('Payment'),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.17,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.0,
                          ),
                          child: Text('Review'),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.5,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * heigg,
                            width: MediaQuery.of(context).size.width * 0.95,
                            padding: EdgeInsets.fromLTRB(
                                0.0,
                                MediaQuery.of(context).size.height * 0.04,
                                MediaQuery.of(context).size.width * 0.0,
                                0),
                            child: SingleChildScrollView(
                              child: IndexedStack(children: [
                                custom.ExpansionTile(
                                  title: Text(
                                    'Show cart details\t\t $costPrice',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.blue),
                                  ),
                                  headerBackgroundColor:
                                      Color.fromRGBO(255, 255, 255, 1),
                                  onExpansionChanged: (value) {
                                    setState(() {
                                      if (visible) {
                                        visible = false;
                                        heigg = 0.5;
                                      } else {
                                        visible = true;
                                        heigg = 0.12;
                                      }
                                    });
                                  },
                                  backgroundColor:
                                      Color.fromRGBO(255, 255, 255, 0.5),
                                  children: [
                                    CartCard(
                                        imagePath: imagePath,
                                        pieceName: pieceName,
                                        tradeMark: tradeMark,
                                        costPrice: costPrice),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(
                                          0.0,
                                          0,
                                          MediaQuery.of(context).size.width *
                                              0.7,
                                          0),
                                      child: Text(
                                        'Discount',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.06,
                                      margin: EdgeInsets.fromLTRB(
                                          10.0,
                                          10.0,
                                          MediaQuery.of(context).size.width *
                                              0.015,
                                          10.0),
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color:
                                                Colors.grey, // set border color
                                            width: 0.5,
                                            style: BorderStyle
                                                .solid), // set border width
                                        // set rounded corner radius
                                      ),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: TextField(
                                              decoration: InputDecoration(
                                                hintText: '',
                                                hintStyle: TextStyle(
                                                  color: Colors.grey,
                                                ),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.5,
                                                0,
                                                0,
                                                0),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                0,
                                                0,
                                                0),
                                            child: ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                onPrimary: Colors.black87,
                                                primary: Colors.grey[300],
                                                minimumSize: Size(44, 40),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 0),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(2)),
                                                ),
                                              ),
                                              child: Text('Apply'),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.001,
                                          0.0, //MediaQuery.of(context).size.height*0.0002,
                                          MediaQuery.of(context).size.width *
                                              0.0,
                                          0.0 // MediaQuery.of(context).size.height*0.06,
                                          ),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.200,
                                      width: MediaQuery.of(context).size.width *
                                          0.9,
                                      color: Color.fromRGBO(249, 249, 255, 1.0),
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                child: Text('Subtotal'),
                                                margin: EdgeInsets.fromLTRB(
                                                  MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.05,
                                                  MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.02,
                                                  MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.57,
                                                  MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01,
                                                ),
                                              ),
                                              Container(
                                                  child: Text('$costPrice')),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                child: Text('Shipping'),
                                                margin: EdgeInsets.fromLTRB(
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        0.05,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.01,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        0.57,
                                                    0.0 // MediaQuery.of(context).size.height*0.06,
                                                    ),
                                              ),
                                              Container(child: Text('FREE')),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                child: Text('Taxes'),
                                                margin: EdgeInsets.fromLTRB(
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        0.05,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.02,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        0.61,
                                                    0.0 // MediaQuery.of(context).size.height*0.06,
                                                    ),
                                              ),
                                              Container(child: Text('$taxes')),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                child: Text('Total'),
                                                margin: EdgeInsets.fromLTRB(
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        0.05,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.02,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        0.62,
                                                    0.0 // MediaQuery.of(context).size.height*0.06,
                                                    ),
                                              ),
                                              Container(child: Text('$total')),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(0.0, 0,
                                MediaQuery.of(context).size.width * 0.85, 0),
                            child: Text(
                              'Email',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: 'youremail@xmail.com',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width * 0.07,
                                    0,
                                    MediaQuery.of(context).size.width * 0.04,
                                    0),
                                child: Text(
                                  'Already have an account?',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(
                                    0.0,
                                    MediaQuery.of(context).size.height * 0.007,
                                    MediaQuery.of(context).size.width * 0.0,
                                    0),
                                child: Text(
                                  'Log in',
                                  style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.78,
                                0),
                            child: Text(
                              'Full Name',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: 'John.k Smith',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(0.0, 0,
                                MediaQuery.of(context).size.width * 0.7, 0),
                            child: Text(
                              'Street Address',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: '1600 Mamak, Ankara, Turkey',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(0.0, 0,
                                MediaQuery.of(context).size.width * 0.53, 0),
                            child: Text(
                              'Apt, Suite, Bldg (optional)',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: 'Unit 512',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(0.0, 0,
                                MediaQuery.of(context).size.width * 0.76, 0),
                            child: Text(
                              'Zip Code',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width * 0.03,
                                    0.0,
                                    0.0,
                                    0.0),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.23,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, // set border color
                                        width: 0.5,
                                        style: BorderStyle
                                            .solid), // set border width
                                    // set rounded corner radius
                                  ),
                                  child: TextField(
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                      hintText: '06540',
                                      hintStyle: TextStyle(
                                        color: Colors.grey,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width * 0.04,
                                    MediaQuery.of(context).size.height * 0.017,
                                    MediaQuery.of(context).size.width * 0.0,
                                    0),
                                child: Row(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.1,
                                          0,
                                          MediaQuery.of(context).size.width *
                                              0.07,
                                          0),
                                      child: Text(
                                        'Chicago, IL',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20.0),
                                      ),
                                    ),
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        onPrimary: Colors.black87,
                                        primary: Colors.grey[300],
                                        minimumSize: Size(36, 36),
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 5),
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                      ),
                                      child: Icon(
                                        Icons.minimize_outlined,
                                        color: Colors.grey[600],
                                      ),
                                    ),
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        onPrimary: Colors.black87,
                                        primary: Colors.grey[300],
                                        minimumSize: Size(36, 36),
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 5),
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                      ),
                                      child: Icon(
                                        Icons.add,
                                        color: Colors.grey[600],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.7,
                                0),
                            child: Text(
                              'Phone Number',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.06,
                                width: MediaQuery.of(context).size.width * 0.7,
                                margin: EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width * 0.03,
                                    MediaQuery.of(context).size.height * 0.01,
                                    MediaQuery.of(context).size.width * 0.03,
                                    MediaQuery.of(context).size.height * 0.03),
                                padding: EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width * 0.04,
                                    0,
                                    0,
                                    0),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey, // set border color
                                      width: 0.5,
                                      style: BorderStyle
                                          .solid), // set border width
                                  // set rounded corner radius
                                ),
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: '(XXX) XXX - XXXX',
                                    hintStyle: TextStyle(
                                      color: Colors.grey,
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.06,
                                width: MediaQuery.of(context).size.width * 0.2,
                                margin: EdgeInsets.fromLTRB(
                                    0.0,
                                    MediaQuery.of(context).size.height * 0.00,
                                    0,
                                    MediaQuery.of(context).size.height * 0.02),
                                padding: EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width * 0.03,
                                    0,
                                    0,
                                    0),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey, // set border color
                                      width: 0.5,
                                      style: BorderStyle
                                          .solid), // set border width
                                  // set rounded corner radius
                                ),
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: 'ext',
                                    hintStyle: TextStyle(
                                      color: Colors.grey,
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                0.0,
                                MediaQuery.of(context).size.height * 0.017,
                                MediaQuery.of(context).size.width * 0.4,
                                0),
                            child: Text(
                              'Another Number',
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 17.0,
                              ),
                            ),
                          ),
                          Visibility(
                            visible: false,
                            child: Column(
                              children: [
                                Text('Phone Number #2'),
                                Row(
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.06,
                                      width: MediaQuery.of(context).size.width *
                                          0.7,
                                      margin: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.03,
                                          MediaQuery.of(context).size.height *
                                              0.03,
                                          MediaQuery.of(context).size.width *
                                              0.03,
                                          MediaQuery.of(context).size.height *
                                              0.03),
                                      padding: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.04,
                                          0,
                                          0,
                                          0),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color:
                                                Colors.grey, // set border color
                                            width: 0.5,
                                            style: BorderStyle
                                                .solid), // set border width
                                        // set rounded corner radius
                                      ),
                                      child: TextField(
                                        decoration: InputDecoration(
                                          hintText: '(XXX) XXX - XXXX',
                                          hintStyle: TextStyle(
                                            color: Colors.grey,
                                          ),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.06,
                                      width: MediaQuery.of(context).size.width *
                                          0.2,
                                      padding: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.03,
                                          0,
                                          0,
                                          0),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color:
                                                Colors.grey, // set border color
                                            width: 0.5,
                                            style: BorderStyle
                                                .solid), // set border width
                                        // set rounded corner radius
                                      ),
                                      child: TextField(
                                        decoration: InputDecoration(
                                          hintText: 'ext',
                                          hintStyle: TextStyle(
                                            color: Colors.grey,
                                          ),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            height: 30.0,
                            color: Colors.grey[700],
                            endIndent: 20.0,
                            indent: 20.0,
                          ),
                          Container(
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width * 0,
                                    MediaQuery.of(context).size.height * 0.02,
                                    MediaQuery.of(context).size.width * 0.5,
                                    MediaQuery.of(context).size.height * 0.01,
                                  ),
                                  child: Text(
                                    'Shipping Method',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.85,
                                      child: ListTile(
                                        title: const Text(
                                            'Free FedEx Ground shipping'),
                                        leading: Radio<ShippingMethod>(
                                          value: ShippingMethod.free,
                                          groupValue: _method,
                                          onChanged: (ShippingMethod value) {
                                            setState(() {
                                              _method = value;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                    Text('data'),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.85,
                                      child: ListTile(
                                        title: const Text(
                                          'FedEx Ground Shipping. 2-3 business days after processing',
                                          softWrap: true,
                                        ),
                                        leading: Radio<ShippingMethod>(
                                          value: ShippingMethod.cheap,
                                          groupValue: _method,
                                          onChanged: (ShippingMethod value) {
                                            setState(() {
                                              _method = value;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                    Text('data'),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.85,
                                      child: ListTile(
                                        title: Text('FedEx One-Day Shipping'),
                                        leading: Radio<ShippingMethod>(
                                          value: ShippingMethod.expensive,
                                          groupValue: _method,
                                          onChanged: (ShippingMethod value) {
                                            setState(() {
                                              _method = value;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                    Text('data'),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.width * 0.209,
              width: MediaQuery.of(context).size.width * 0.99,
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.059,
                        MediaQuery.of(context).size.height * 0.017,
                        MediaQuery.of(context).size.width * 0.05,
                        0),
                    child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          'Back',
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 17.0,
                          ),
                        )),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.66,
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.02,
                        MediaQuery.of(context).size.height * 0.03,
                        MediaQuery.of(context).size.width * 0.0,
                        MediaQuery.of(context).size.width * 0.03),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Color.fromRGBO(0, 160, 227, 1)),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/Payment');
                      },
                      color: Colors.lightBlue,
                      textColor: Colors.white,
                      child: Text("CONTINUE TO PAYMENT",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 15,
                          )),
                    ),
                  ),
                ],
              ),
            ),
            copyRights(),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      drawer: MyDrawer(),
    );
  }
}
