import 'dart:ui';

import 'package:e_commerce_sport_shirts/accessories/Cards/MyDrawer.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/copyRights.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/myAppBar.dart';
import 'package:e_commerce_sport_shirts/customExpansionTile.dart' as custom;
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

void main() => runApp(MaterialApp(
      home: ProductDetails(),
    ));

class ProductDetails extends StatefulWidget {
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    String appTitle = 'FIFA World Cup Kits';
    List<String> myList = ['00.jpg', '01.png'];
    int currentPos = 0;
    var usd = Currency.create('USD', 0);
    Money costPrice = Money.fromInt(165, usd);

    return Scaffold(
      appBar: myAppBar(title: appTitle),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height * 0.4,
                width: MediaQuery.of(context).size.width,
                color: Color.fromRGBO(243, 212, 212, 0.21176470588235294),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.4,
                  child: IndexedStack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset('assets/images/00.jpg', fit: BoxFit.fill),
                      Positioned(
                        height: 20.0,
                        child: Container(
                          width: 8.0,
                          height: 8.0,
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 50),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: currentPos == 0
                                ? Color.fromRGBO(255, 0, 0, 1.0)
                                : Color.fromRGBO(
                                    208, 191, 191, 0.8666666666666667),
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
            Text(
              'FRANCE AUTHENTIC\n'
              'HOME JERSY 2018',
              style: TextStyle(
                fontSize: 20.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'NIKE',
              style: TextStyle(
                fontSize: 25.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              '$costPrice',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            Divider(
              height: 10.0,
              color: Colors.grey[700],
              endIndent: 20.0,
              indent: 20.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.255,
              width: MediaQuery.of(context).size.width * 1,
              padding: EdgeInsets.fromLTRB(
                  0.0,
                  MediaQuery.of(context).size.height * 0.04,
                  MediaQuery.of(context).size.width * 0.0,
                  0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.00,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.00,
                          0.0),
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            child: Text(
                              'Size',
                              style: TextStyle(
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.05,
                                0.0,
                                MediaQuery.of(context).size.width * 0.01,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'S',
                                style: TextStyle(
                                  fontSize: 25.0,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.00,
                                0.0,
                                MediaQuery.of(context).size.width * 0.01,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'M',
                                style: TextStyle(
                                  fontSize: 25.0,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.00,
                                0.0,
                                MediaQuery.of(context).size.width * 0.01,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'L',
                                style: TextStyle(
                                  fontSize: 25.0,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                0.0,
                                MediaQuery.of(context).size.width * 0.01,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'XL',
                                style: TextStyle(
                                  fontSize: 25.0,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.02,
                                0.0,
                                MediaQuery.of(context).size.width * 0.01,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'XXL',
                                style: TextStyle(
                                  fontSize: 25.0,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.00,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.00,
                          0.0),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                            child: Text(
                              'Kit',
                              style: TextStyle(
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.08,
                                0.0,
                                MediaQuery.of(context).size.width * 0.05,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'HOME',
                                style: TextStyle(
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.02,
                                0.0,
                                MediaQuery.of(context).size.width * 0.05,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'AWAY',
                                style: TextStyle(
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.02,
                                0.0,
                                MediaQuery.of(context).size.width * 0.05,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(40, 40),
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Text(
                                'THIRD',
                                style: TextStyle(
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.00,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.00,
                          0.0),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                            child: Text(
                              'Qty',
                              style: TextStyle(
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.04,
                                0.0,
                                MediaQuery.of(context).size.width * 0.03,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                //onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(36, 36),
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Icon(
                                Icons.minimize,
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Text(
                            '1',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.03,
                                0.0,
                                MediaQuery.of(context).size.width * 0.03,
                                0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.grey[300],
                                minimumSize: Size(36, 36),
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              child: Icon(
                                Icons.add,
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: custom.ExpansionTile(
                        title: Text(
                          'Customize Your Jersey',
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                        headerBackgroundColor: Color.fromRGBO(255, 255, 255, 1),
                        onExpansionChanged: (value) {
                          setState(() {});
                        },
                        backgroundColor: Color.fromRGBO(255, 255, 255, 0.5),
                        children: [],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.76,
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.0,
                          MediaQuery.of(context).size.height * 0.03,
                          MediaQuery.of(context).size.width * 0.0,
                          MediaQuery.of(context).size.width * 0.03),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side:
                              BorderSide(color: Color.fromRGBO(0, 160, 227, 1)),
                        ),
                        onPressed: () {},
                        color: Colors.lightBlue,
                        textColor: Colors.white,
                        child: Text("ADD TO CART",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 15,
                            )),
                      ),
                    ),
                    Divider(
                      height: 10.0,
                      color: Colors.grey[700],
                      endIndent: 20.0,
                      indent: 20.0,
                    ),
                    Container(
                      child: custom.ExpansionTile(
                        title: Text(
                          'Product Details',
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                        headerBackgroundColor: Color.fromRGBO(255, 255, 255, 1),
                        onExpansionChanged: (value) {
                          setState(() {});
                        },
                        backgroundColor: Color.fromRGBO(255, 255, 255, 0.5),
                        children: [],
                      ),
                    ),
                    Container(
                      child: custom.ExpansionTile(
                        title: Text(
                          'Shipping & Returns',
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                        headerBackgroundColor: Color.fromRGBO(255, 255, 255, 1),
                        onExpansionChanged: (value) {
                          setState(() {});
                        },
                        backgroundColor: Color.fromRGBO(255, 255, 255, 0.5),
                        children: [],
                      ),
                    ),
                    Divider(
                      height: 10.0,
                      color: Colors.grey[700],
                      endIndent: 20.0,
                      indent: 20.0,
                    ),
                  ],
                ),
              ),
            ),
            copyRights(),
          ],
        ),
      ),
      drawer: MyDrawer(),
    );
  }
}
