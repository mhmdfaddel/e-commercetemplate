import 'package:e_commerce_sport_shirts/accessories/Cards/MyDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/accessories/Cards/myAppBar.dart';

import '../Cards/HorCard.dart';
import '../accessories/Cards/copyRights.dart';

void main() => runApp(MaterialApp(
      home: ProductSearch(),
    ));

class ProductSearch extends StatefulWidget {
  @override
  _ProductSearch createState() => _ProductSearch();
}

class _ProductSearch extends State<ProductSearch> {
  final appTitle = 'FIFA World Cup Kits';

  @override
  Widget build(BuildContext context) {
    var usd = Currency.create('USD', 0);
    Money costPrice = Money.fromInt(165, usd);
    String tradeMark = 'NIKE';
    String pieceName = 'BRASIL AUTHENTIC\nJERSEY 2018';
    String imagePath = 'assets/images/01.png';
    List<HorCard> cards = [
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        pieceName: pieceName,
        tradeMark: tradeMark,
      ),
    ];

    return Scaffold(
      appBar: myAppBar(
        title: this.appTitle,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              children: [
                Text(
                  'Authentic World Cup Kits',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 30.0,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.06,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  color: Colors.grey, // set border color
                  width: 0.5,
                  style: BorderStyle.solid), // set border width
              // set rounded corner radius
            ),
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Search Kits',
                hintStyle: TextStyle(
                  color: Colors.grey,
                ),
                border: InputBorder.none,
                icon: Icon(Icons.search),
              ),
            ),
          ),
          Container(
            // height: MediaQuery.of(context).size.height*0.055,
            // width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.02,
                MediaQuery.of(context).size.width * 0.02,
                0,
                MediaQuery.of(context).size.width * 0.02),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.view_list,
                    color: Colors.grey[600],
                    size: 30.0,
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.view_module_sharp,
                    color: Colors.grey[600],
                    size: 30.0,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.49, 0, 0, 0),
                  child: Text(
                    'FILTER',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.04, 0, 0, 0),
                  child: Text(
                    'SORT',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.5848,
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                    itemCount: cards.length,
                    itemBuilder: (context, index) {
                      return Card(
                        shadowColor: Colors.white,
                        margin: EdgeInsets.all(0.8),
                        elevation: 15.0,
                        child: cards[index],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          copyRights(),
        ],
      ),
      backgroundColor: Colors.white,
      drawer: MyDrawer(),
    );
  }
}
