import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/Cards/CartCard.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/MyDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/accessories/Cards/myAppBar.dart';

import '../accessories/Cards/copyRights.dart';

void main() => runApp(MaterialApp(
      home: CartPage(),
    ));

class CartPage extends StatefulWidget {
  @override
  _CartPage createState() => _CartPage();
}

class _CartPage extends State<CartPage> {
  final appTitle = 'FIFA World Cup Kits';
  Money costPrice = Money.fromInt(165, Currency.create('USD', 0));
  String tradeMark = 'NIKE';
  String pieceName = 'BRASIL AUTHENTIC\nJERSEY 2018';
  String imagePath = 'assets/images/01.png';
  Money taxes = Money.from(11.5, Currency.create('USD', 0));
  Money total = Money.from(176.5, Currency.create('USD', 0));
  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.height / 752);
    return Scaffold(
      appBar: myAppBar(
        title: this.appTitle,
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0,
              MediaQuery.of(context).size.height * 0.02,
              MediaQuery.of(context).size.width * 0.58,
              MediaQuery.of(context).size.height * 0.01,
            ),
            child: Text(
              'Your Cart',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 30.0,
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.441,
            width: MediaQuery.of(context).size.width * 1,
            //color: Colors.black45,

            padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0.001,
              MediaQuery.of(context).size.height * 0.002,
              MediaQuery.of(context).size.width * 0.0,
              MediaQuery.of(context).size.height * 0.03,
            ),
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    CartCard(
                      imagePath: imagePath,
                      pieceName: pieceName,
                      tradeMark: tradeMark,
                      costPrice: costPrice,
                    ),
                    CartCard(
                        imagePath: imagePath,
                        pieceName: pieceName,
                        tradeMark: tradeMark),
                    CartCard(
                        imagePath: imagePath,
                        pieceName: pieceName,
                        tradeMark: tradeMark),
                    // CartCard(imagePath: imagePath, pieceName: pieceName, tradeMark: tradeMark),
                    // CartCard(imagePath: imagePath, pieceName: pieceName, tradeMark: tradeMark),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.001,
                0.0, //MediaQuery.of(context).size.height*0.0002,
                MediaQuery.of(context).size.width * 0.0,
                0.0 // MediaQuery.of(context).size.height*0.06,
                ),
            height: MediaQuery.of(context).size.height * 0.200,
            width: MediaQuery.of(context).size.width * 0.9,
            color: Color.fromRGBO(249, 249, 255, 1.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      child: Text('Subtotal'),
                      margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.05,
                        MediaQuery.of(context).size.height * 0.02,
                        MediaQuery.of(context).size.width * 0.57,
                        MediaQuery.of(context).size.height * 0.01,
                      ),
                    ),
                    Container(child: Text('$costPrice')),
                  ],
                ),
                Row(
                  children: [
                    Container(
                      child: Text('Shipping'),
                      margin: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.05,
                          MediaQuery.of(context).size.height * 0.01,
                          MediaQuery.of(context).size.width * 0.57,
                          0.0 // MediaQuery.of(context).size.height*0.06,
                          ),
                    ),
                    Container(child: Text('-')),
                  ],
                ),
                Row(
                  children: [
                    Container(
                      child: Text('Taxes'),
                      margin: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.05,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.6,
                          0.0 // MediaQuery.of(context).size.height*0.06,
                          ),
                    ),
                    Container(child: Text('-')),
                  ],
                ),
                Row(
                  children: [
                    Container(
                      child: Text('Total'),
                      margin: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.05,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.57,
                          0.0 // MediaQuery.of(context).size.height*0.06,
                          ),
                    ),
                    Container(child: Text('$costPrice')),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.1,
            width: MediaQuery.of(context).size.width * 0.99,
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.00,
                      MediaQuery.of(context).size.height * 0.017,
                      MediaQuery.of(context).size.width * 0.00,
                      0),
                  child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Back',
                        style: TextStyle(
                          color: Colors.blue,
                          fontSize: 17.0,
                        ),
                      )),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.76,
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.12,
                      MediaQuery.of(context).size.height * 0.03,
                      MediaQuery.of(context).size.width * 0.0,
                      MediaQuery.of(context).size.width * 0.03),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Color.fromRGBO(0, 160, 227, 1)),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/ShippingReview');
                    },
                    color: Colors.lightBlue,
                    textColor: Colors.white,
                    child: Text("CONTINUE TO CHECK OUT",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15,
                        )),
                  ),
                ),
              ],
            ),
          ),
          copyRights(),
        ],
      ),
      backgroundColor: Colors.white,
      drawer: MyDrawer(),
    );
  }
}
// Text('Shipping'),
// Text('Taxes'),
// Text('Total'),
