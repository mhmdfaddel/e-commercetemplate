import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/accessories/Cards/myAppBar.dart';

import '../accessories/Cards/copyRights.dart';

void main() => runApp(MaterialApp(
      home: ThanksPage(),
    ));

class ThanksPage extends StatefulWidget {
  @override
  _ThanksPage createState() => _ThanksPage();
}

class _ThanksPage extends State<ThanksPage> {
  final appTitle = 'FIFA World Cup Kits';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar(
        title: this.appTitle,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.8930,
              child: Column(
                children: [
                  Text(
                    'Thank You, your order has been placed',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 30.0,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.04,
                  ),
                  Text(
                    'Your reference number is *abc123. \nAn order confirmation email has been sent to \n',
                    style: TextStyle(
                      fontSize: 17.0,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        0.0, 0, MediaQuery.of(context).size.width * 0.35, 0),
                    child: Text(
                      'mhmdfaddel@gmail.com',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        0.0,
                        MediaQuery.of(context).size.height * 0.02,
                        MediaQuery.of(context).size.width * 0.17,
                        0),
                    child: Text(
                      'Send confirmation to another email.',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                  Divider(
                    height: MediaQuery.of(context).size.height * 0.05,
                    color: Colors.grey[700],
                    endIndent: 3.0,
                    indent: 10.0,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        0.0,
                        MediaQuery.of(context).size.height * 0.01,
                        MediaQuery.of(context).size.width * 0.17,
                        0),
                    child: Text(
                      'Create an account for easy checkout',
                      style: TextStyle(
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        0.0,
                        MediaQuery.of(context).size.height * 0.03,
                        MediaQuery.of(context).size.width * 0.72,
                        MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      'Password',
                      style: TextStyle(
                        fontSize: 11.0,
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                      color: Colors.white,

                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.03,
                          0.0,
                          0.0,
                          0),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: 'Minimum 8 characters long',
                          hintStyle: TextStyle(
                            color: Colors.grey[400],
                          ),
                          border: InputBorder.none,
                        ),
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.988,
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.009,
                        MediaQuery.of(context).size.height * 0.03,
                        MediaQuery.of(context).size.width * 0.33,
                        MediaQuery.of(context).size.width * 0.03),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Color.fromRGBO(0, 160, 227, 1)),
                      ),
                      onPressed: () {},
                      color: Colors.lightBlue,
                      textColor: Colors.white,
                      child: Text("Create account",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                          )),
                    ),
                  ),
                  Divider(
                    height: MediaQuery.of(context).size.height * 0.04,
                    color: Colors.grey[700],
                    endIndent: 3.0,
                    indent: 10.0,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        0.0,
                        MediaQuery.of(context).size.height * 0.017,
                        MediaQuery.of(context).size.width * 0.4,
                        0),
                    child: Text(
                      'Continue browsing kits',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            copyRights(),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      drawer: Drawer(),
    );
  }
}
