import 'package:e_commerce_sport_shirts/Carts/ReviewCard.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/MyDrawer.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/copyRights.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/accessories/Cards/myAppBar.dart';

void main() => runApp(MaterialApp(
      home: CheckoutReview(),
    ));

class CheckoutReview extends StatefulWidget {
  @override
  _CheckoutReview createState() => _CheckoutReview();
}

enum ShippingMethod { free, cheap, expensive }

class _CheckoutReview extends State<CheckoutReview> {
  final appTitle = 'FIFA World Cup Kits';
  Money costPrice = Money.fromInt(165, Currency.create('USD', 0));
  Money taxes = Money.from(11.55, Currency.create('USD', 0));
  Money total = Money.from(176.55, Currency.create('USD', 0));
  String tradeMark = 'NIKE';
  String pieceName = 'BRASIL AUTHENTIC\nJERSEY 2018';
  String imagePath = 'assets/images/01.png';
  StepperType stepperType = StepperType.horizontal;

  ShippingMethod _method = ShippingMethod.free;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar(
        title: this.appTitle,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0,
                MediaQuery.of(context).size.height * 0.02,
                MediaQuery.of(context).size.width * 0.58,
                MediaQuery.of(context).size.height * 0.01,
              ),
              child: Text(
                'Checkout',
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 30.0,
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.641,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 40.0,
                        height: 40.0,
                        padding: EdgeInsets.all(
                            MediaQuery.of(context).size.width * 0.0),
                        decoration: new BoxDecoration(
                          color: Colors.lightBlue[300],
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.check,
                          size: 25.0,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                        width: 70.0,
                        child: Container(
                          color: Colors.grey[300],
                        ),
                      ),
                      Row(
                        children: [
                          Container(
                            width: 40.0,
                            height: 40.0,
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.width * 0.0),
                            decoration: new BoxDecoration(
                              color: Colors.lightBlue[300],
                              shape: BoxShape.circle,
                            ),
                            child: Icon(
                              Icons.check,
                              size: 25.0,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            height: 1.0,
                            width: 70.0,
                            child: Container(
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 40.0,
                            height: 40.0,
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.width * 0.03),
                            decoration: new BoxDecoration(
                              color: Colors.lightBlue,
                              shape: BoxShape.circle,
                            ),
                            child: Text(
                              '3',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.08,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.0,
                          ),
                          child: Text('Shipping'),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.17,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.0,
                          ),
                          child: Text('Payment'),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.17,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.0,
                          ),
                          child: Text('Review'),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.241,
                    width: MediaQuery.of(context).size.width * 1,
                    //color: Colors.black45,

                    padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.001,
                      MediaQuery.of(context).size.height * 0.002,
                      MediaQuery.of(context).size.width * 0.0,
                      MediaQuery.of(context).size.height * 0.00,
                    ),
                    child: SafeArea(
                      child: Scrollbar(
                        isAlwaysShown: true,
                        thickness: 5.0,
                        child: SingleChildScrollView(
                          child: Container(
                            child: Column(
                              children: [
                                ReviewCard(
                                  imagePath: imagePath,
                                  pieceName: pieceName,
                                  tradeMark: tradeMark,
                                  costPrice: costPrice,
                                ),
                                ReviewCard(
                                  imagePath: imagePath,
                                  pieceName: pieceName,
                                  tradeMark: tradeMark,
                                  costPrice: costPrice,
                                ),
                                ReviewCard(
                                  imagePath: imagePath,
                                  pieceName: pieceName,
                                  tradeMark: tradeMark,
                                  costPrice: costPrice,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Divider(
                    height: 20.0,
                    indent: 10.0,
                    endIndent: 20.0,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.26,
                    width: MediaQuery.of(context).size.width * 1,
                    //color: Colors.black45,

                    padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.001,
                      MediaQuery.of(context).size.height * 0.002,
                      MediaQuery.of(context).size.width * 0.0,
                      MediaQuery.of(context).size.height * 0.00,
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.fromLTRB(
                                            MediaQuery.of(context).size.width *
                                                0.08,
                                            MediaQuery.of(context).size.height *
                                                0.0,
                                            MediaQuery.of(context).size.width *
                                                0.3,
                                            MediaQuery.of(context).size.height *
                                                0.0),
                                        child:
                                            Text('Order Confirmation sent to')),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.06,
                                          MediaQuery.of(context).size.height *
                                              0.0,
                                          MediaQuery.of(context).size.width *
                                              0.0,
                                          MediaQuery.of(context).size.height *
                                              0.0),
                                      child: Text(
                                        ' EDIT',
                                        style: TextStyle(
                                          color: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.width * 0.07,
                                      MediaQuery.of(context).size.height * 0.01,
                                      MediaQuery.of(context).size.width * 0.5,
                                      MediaQuery.of(context).size.height * 0.0),
                                  child: Text('mhmdfaddel@gmail.com'),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.fromLTRB(
                                            MediaQuery.of(context).size.width *
                                                0.08,
                                            MediaQuery.of(context).size.height *
                                                0.05,
                                            MediaQuery.of(context).size.width *
                                                0.3,
                                            MediaQuery.of(context).size.height *
                                                0.0),
                                        child: Text('Shipping to')),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.3,
                                          MediaQuery.of(context).size.height *
                                              0.04,
                                          MediaQuery.of(context).size.width *
                                              0.0,
                                          MediaQuery.of(context).size.height *
                                              0.0),
                                      child: Text(
                                        ' EDIT',
                                        style: TextStyle(
                                          color: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.width * 0.01,
                                      MediaQuery.of(context).size.height * 0.01,
                                      MediaQuery.of(context).size.width * 0.55,
                                      MediaQuery.of(context).size.height * 0.0),
                                  child: Text(
                                      'Ankara, Turkey\nŞafakTepe, Mamak,\nAnkara, Turkey\n(531) 497 7947'),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.fromLTRB(
                                            MediaQuery.of(context).size.width *
                                                0.08,
                                            MediaQuery.of(context).size.height *
                                                0.05,
                                            MediaQuery.of(context).size.width *
                                                0.3,
                                            MediaQuery.of(context).size.height *
                                                0.0),
                                        child: Text('Payment Method')),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.2,
                                          MediaQuery.of(context).size.height *
                                              0.04,
                                          MediaQuery.of(context).size.width *
                                              0.0,
                                          MediaQuery.of(context).size.height *
                                              0.0),
                                      child: Text(
                                        ' EDIT',
                                        style: TextStyle(
                                          color: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.width * 0.0,
                                      MediaQuery.of(context).size.height * 0.01,
                                      MediaQuery.of(context).size.width * 0.6,
                                      MediaQuery.of(context).size.height * 0.0),
                                  child: Text('ending in 8765'),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.fromLTRB(
                                            MediaQuery.of(context).size.width *
                                                0.08,
                                            MediaQuery.of(context).size.height *
                                                0.05,
                                            MediaQuery.of(context).size.width *
                                                0.3,
                                            MediaQuery.of(context).size.height *
                                                0.0),
                                        child: Text('Billing Address')),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          MediaQuery.of(context).size.width *
                                              0.2,
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                          MediaQuery.of(context).size.width *
                                              0.0,
                                          MediaQuery.of(context).size.height *
                                              0.0),
                                      child: Text(
                                        ' EDIT',
                                        style: TextStyle(
                                          color: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.width * 0.0,
                                      MediaQuery.of(context).size.height * 0.01,
                                      MediaQuery.of(context).size.width * 0.43,
                                      MediaQuery.of(context).size.height * 0.0),
                                  child: Text('Same as shipping address'),
                                ),
                                Divider(
                                  height: 50.0,
                                  indent: 10.0,
                                  endIndent: 20.0,
                                ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.width * 0.001,
                                      0.0, //MediaQuery.of(context).size.height*0.0002,
                                      MediaQuery.of(context).size.width * 0.0,
                                      0.0 // MediaQuery.of(context).size.height*0.06,
                                      ),
                                  height: MediaQuery.of(context).size.height *
                                      0.200,
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  color: Color.fromRGBO(249, 249, 255, 1.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Subtotal'),
                                            margin: EdgeInsets.fromLTRB(
                                              MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.05,
                                              MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.02,
                                              MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.57,
                                              MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01,
                                            ),
                                          ),
                                          Container(child: Text('$costPrice')),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Shipping'),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01,
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.57,
                                                0.0 // MediaQuery.of(context).size.height*0.06,
                                                ),
                                          ),
                                          Container(child: Text('FREE')),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Expected Delivery'),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02,
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.37,
                                                0.0 // MediaQuery.of(context).size.height*0.06,
                                                ),
                                          ),
                                          Container(child: Text('Apr 20 - 28')),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Taxes'),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02,
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.61,
                                                0.0 // MediaQuery.of(context).size.height*0.06,
                                                ),
                                          ),
                                          Container(child: Text('$taxes')),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Total'),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02,
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.62,
                                                0.0 // MediaQuery.of(context).size.height*0.06,
                                                ),
                                          ),
                                          Container(child: Text('$total')),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.width * 0.209,
              width: MediaQuery.of(context).size.width * 0.99,
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.059,
                        MediaQuery.of(context).size.height * 0.017,
                        MediaQuery.of(context).size.width * 0.05,
                        0),
                    child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          'Back',
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 17.0,
                          ),
                        )),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.66,
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.02,
                        MediaQuery.of(context).size.height * 0.03,
                        MediaQuery.of(context).size.width * 0.0,
                        MediaQuery.of(context).size.width * 0.03),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Color.fromRGBO(0, 160, 227, 1)),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/Thanks');
                      },
                      color: Colors.lightBlue,
                      textColor: Colors.white,
                      child: Text("FINISH CHECKOUT",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 15,
                          )),
                    ),
                  ),
                ],
              ),
            ),
            copyRights(),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      drawer: MyDrawer(),
    );
  }
}

