import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/Cards/CartCard.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/MyDrawer.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/copyRights.dart';
import 'package:e_commerce_sport_shirts/customExpansionTile.dart' as custom;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/accessories/Cards/myAppBar.dart';

void main() => runApp(MaterialApp(
      home: PaymentPage(),
    ));

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPage createState() => _PaymentPage();
}

enum ShippingMethod { free, cheap, expensive }

class _PaymentPage extends State<PaymentPage> {
  final appTitle = 'FIFA World Cup Kits';
  Money costPrice = Money.fromInt(165, Currency.create('USD', 0));
  String tradeMark = 'NIKE';
  String pieceName = 'BRASIL AUTHENTIC\nJERSEY 2018';
  String imagePath = 'assets/images/01.png';
  int _currentStep = 1;
  StepperType stepperType = StepperType.horizontal;
  var heigg = 0.12;
  ShippingMethod _method = ShippingMethod.free;
  bool visible = true;
  Money taxes = Money.from(11.5, Currency.create('USD', 0));
  Money total = Money.from(176.5, Currency.create('USD', 0));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar(
        title: this.appTitle,
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0,
              MediaQuery.of(context).size.height * 0.02,
              MediaQuery.of(context).size.width * 0.58,
              MediaQuery.of(context).size.height * 0.01,
            ),
            child: Text(
              'Checkout',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 30.0,
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.641,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 40.0,
                      height: 40.0,
                      padding: EdgeInsets.all(
                          MediaQuery.of(context).size.width * 0.0),
                      decoration: new BoxDecoration(
                        color: Colors.lightBlue[300],
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.check,
                        size: 25.0,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 1.0,
                      width: 70.0,
                      child: Container(
                        color: Colors.grey[300],
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          width: 40.0,
                          height: 40.0,
                          padding: EdgeInsets.all(
                              MediaQuery.of(context).size.width * 0.03),
                          decoration: new BoxDecoration(
                            color: Colors.lightBlue,
                            shape: BoxShape.circle,
                          ),
                          child: Text(
                            '2',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        SizedBox(
                          height: 1.0,
                          width: 70.0,
                          child: Container(
                            color: Colors.grey,
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: 40.0,
                          height: 40.0,
                          padding: EdgeInsets.all(
                              MediaQuery.of(context).size.width * 0.03),
                          decoration: new BoxDecoration(
                            color: Colors.grey,
                            shape: BoxShape.circle,
                          ),
                          child: Text(
                            '3',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.08,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.0,
                          MediaQuery.of(context).size.height * 0.0,
                        ),
                        child: Text('Shipping'),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.17,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.0,
                          MediaQuery.of(context).size.height * 0.0,
                        ),
                        child: Text('Payment'),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.17,
                          MediaQuery.of(context).size.height * 0.02,
                          MediaQuery.of(context).size.width * 0.0,
                          MediaQuery.of(context).size.height * 0.0,
                        ),
                        child: Text('Review'),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * heigg,
                        width: MediaQuery.of(context).size.width * 0.95,
                        padding: EdgeInsets.fromLTRB(
                            0.0,
                            MediaQuery.of(context).size.height * 0.04,
                            MediaQuery.of(context).size.width * 0.0,
                            0),
                        child: SingleChildScrollView(
                          child: IndexedStack(children: [
                            custom.ExpansionTile(
                              title: Text(
                                'Show cart details\t\t $costPrice',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.blue),
                              ),
                              headerBackgroundColor:
                                  Color.fromRGBO(255, 255, 255, 1),
                              onExpansionChanged: (value) {
                                setState(() {
                                  if (visible) {
                                    visible = false;
                                    heigg = 0.5;
                                  } else {
                                    visible = true;
                                    heigg = 0.12;
                                  }
                                });
                              },
                              backgroundColor:
                                  Color.fromRGBO(255, 255, 255, 0.5),
                              children: [
                                CartCard(
                                    imagePath: imagePath,
                                    pieceName: pieceName,
                                    tradeMark: tradeMark,
                                    costPrice: costPrice),
                                Container(
                                  padding: EdgeInsets.fromLTRB(
                                      0.0,
                                      0,
                                      MediaQuery.of(context).size.width * 0.7,
                                      0),
                                  child: Text(
                                    'Discount',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.06,
                                  margin: EdgeInsets.fromLTRB(
                                      10.0,
                                      10.0,
                                      MediaQuery.of(context).size.width * 0.015,
                                      10.0),
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, // set border color
                                        width: 0.5,
                                        style: BorderStyle
                                            .solid), // set border width
                                    // set rounded corner radius
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: TextField(
                                          decoration: InputDecoration(
                                            hintText: '',
                                            hintStyle: TextStyle(
                                              color: Colors.grey,
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.fromLTRB(
                                            MediaQuery.of(context).size.width *
                                                0.5,
                                            0,
                                            0,
                                            0),
                                        margin: EdgeInsets.fromLTRB(
                                            MediaQuery.of(context).size.width *
                                                0.05,
                                            0,
                                            0,
                                            0),
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            onPrimary: Colors.black87,
                                            primary: Colors.grey[300],
                                            minimumSize: Size(44, 40),
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 0),
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(2)),
                                            ),
                                          ),
                                          child: Text('Apply'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.width * 0.001,
                                      0.0, //MediaQuery.of(context).size.height*0.0002,
                                      MediaQuery.of(context).size.width * 0.0,
                                      0.0 // MediaQuery.of(context).size.height*0.06,
                                      ),
                                  height: MediaQuery.of(context).size.height *
                                      0.200,
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  color: Color.fromRGBO(249, 249, 255, 1.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Subtotal'),
                                            margin: EdgeInsets.fromLTRB(
                                              MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.05,
                                              MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.02,
                                              MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.57,
                                              MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01,
                                            ),
                                          ),
                                          Container(child: Text('$costPrice')),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Shipping'),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01,
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.57,
                                                0.0 // MediaQuery.of(context).size.height*0.06,
                                                ),
                                          ),
                                          Container(child: Text('FREE')),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Taxes'),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02,
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.61,
                                                0.0 // MediaQuery.of(context).size.height*0.06,
                                                ),
                                          ),
                                          Container(child: Text('$taxes')),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            child: Text('Total'),
                                            margin: EdgeInsets.fromLTRB(
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02,
                                                MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.62,
                                                0.0 // MediaQuery.of(context).size.height*0.06,
                                                ),
                                          ),
                                          Container(child: Text('$total')),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ]),
                        ),
                      ),
                      Visibility(
                        visible: visible,
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(0.0, 0,
                                  MediaQuery.of(context).size.width * 0.65, 0),
                              child: Text(
                                'Credit or Debit Card',
                                style: TextStyle(color: Colors.grey),
                              ),
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.06,
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: Colors.grey, // set border color
                                    width: 0.5,
                                    style:
                                        BorderStyle.solid), // set border width
                                // set rounded corner radius
                              ),
                              child: TextField(
                                decoration: InputDecoration(
                                  hintText: '1234 5678 9012 3456',
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  border: InputBorder.none,
                                  icon: Icon(Icons.credit_card),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(0.0, 0,
                                  MediaQuery.of(context).size.width * 0.15, 0),
                              child: Text(
                                'Enter credit card number, expire date & CVV number',
                                style: TextStyle(color: Colors.grey),
                              ),
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.06,
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: Colors.grey, // set border color
                                    width: 0.5,
                                    style:
                                        BorderStyle.solid), // set border width
                                // set rounded corner radius
                              ),
                              child: Row(
                                // Replace with a Row for horizontal icon + text
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image(
                                    image:
                                        AssetImage('assets/images/paypal.png'),
                                    height: 20.0,
                                    width: 20.0,
                                  ),
                                  Text(
                                    "  PAYPAL",
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.06,
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: Colors.grey, // set border color
                                    width: 0.5,
                                    style:
                                        BorderStyle.solid), // set border width
                                // set rounded corner radius
                              ),
                              child: Row(
                                // Replace with a Row for horizontal icon + text
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image(
                                    image:
                                        AssetImage('assets/images/apple.png'),
                                    height: 20.0,
                                    width: 20.0,
                                  ),
                                  Text(
                                    "  APPLE PAY",
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.width * 0.209,
            width: MediaQuery.of(context).size.width * 0.99,
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.059,
                      MediaQuery.of(context).size.height * 0.017,
                      MediaQuery.of(context).size.width * 0.05,
                      0),
                  child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Back',
                        style: TextStyle(
                          color: Colors.blue,
                          fontSize: 17.0,
                        ),
                      )),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.66,
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.02,
                      MediaQuery.of(context).size.height * 0.03,
                      MediaQuery.of(context).size.width * 0.0,
                      MediaQuery.of(context).size.width * 0.03),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Color.fromRGBO(0, 160, 227, 1)),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/CheckoutReview');
                    },
                    color: Colors.lightBlue,
                    textColor: Colors.white,
                    child: Text("CONTINUE TO REVIEW",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15,
                        )),
                  ),
                ),
              ],
            ),
          ),
          copyRights(),
        ],
      ),
      backgroundColor: Colors.white,
      drawer: MyDrawer(),
    );
  }
}
