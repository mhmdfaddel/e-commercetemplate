import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

import '../accessories/Cards/myAppBar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final appTitle = 'FIFA World Cup Kits';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomePage(appTitle: appTitle),
    );
  }
}

class MyHomePage extends StatelessWidget {
  String appTitle = 'FIFA World Cup Kits';

  MyHomePage({Key key, this.appTitle}) : super(key: key);

  List<String> myList = ['00.jpg', '01.png'];

  @override
  Widget build(BuildContext context) {
    int currentPos = 0;
    var usd = Currency.create('USD', 0);
    Money costPrice = Money.fromInt(165, usd);

    return Scaffold(
      appBar: myAppBar(title: this.appTitle),
      body: Container(
        child: Column(
          children: <Widget>[
            CarouselSlider(
              items: myList.map((i) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Builder(
                      builder: (BuildContext context) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          height: 350,
                          margin: EdgeInsets.symmetric(horizontal: 3.0),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/images/$i'),
                              fit: BoxFit.fill,
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: myList.map((url) {
                              int index = myList.indexOf(url);
                              return Container(
                                width: 8.0,
                                height: 8.0,
                                margin: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 2.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: currentPos == index
                                      ? Color.fromRGBO(
                                          203, 175, 175, 0.7176470588235294)
                                      : Color.fromRGBO(
                                          208, 191, 191, 0.8666666666666667),
                                ),
                              );
                            }).toList(),
                          ),
                        );
                      },
                    ),
                  ],
                );
              }).toList(),
              options: CarouselOptions(
                height: 360,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: false,
              ),
            ),
            Text(
              'FRANCE AUTHENTIC\n'
              'HOME JERSY 2018',
              style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'NIKE',
              style: TextStyle(
                fontSize: 25.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              costPrice.toString(),
              style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            Divider(
              height: 10.0,
              color: Colors.grey[700],
              endIndent: 20.0,
              indent: 20.0,
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                  child: Text(
                    'Size',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                FlatButton(
                  child: Text(
                    'LogIn',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  onPressed: () {},
                ),
                FlatButton(
                  child: Text(
                    'LogIn',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  onPressed: () {},
                ),
                FlatButton(
                  child: Text(
                    'LogIn',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  onPressed: () {},
                ),
                FlatButton(
                  child: Text(
                    'LogIn',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  onPressed: () {},
                ),
                FlatButton(
                  child: Text(
                    'LogIn',
                    style: TextStyle(fontSize: 10.0),
                  ),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  onPressed: () {},
                ),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                  child: Text(
                    'Kit',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                FlatButton(
                  textColor: Colors.black,
                  height: 20.0,
                  child: Text(
                    'HOME',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                FlatButton(
                  textColor: Colors.black,
                  height: 20.0,
                  child: Text(
                    'AWAY',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                FlatButton(
                  textColor: Colors.black,
                  height: 20.0,
                  child: Text(
                    'THIRD',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                  child: Text(
                    'Qty',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                FlatButton(
                    textColor: Colors.black,
                    height: 20.0,
                    child: GestureDetector(
                      onTap: () {},
                      child: Icon(
                        Icons.minimize,
                        color: Colors.grey[600],
                        size: 20.0,
                      ),
                    )),
                Text(
                  '1',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
                FlatButton(
                    textColor: Colors.black,
                    focusColor: Colors.grey,
                    height: 20.0,
                    child: GestureDetector(
                      onTap: () {},
                      child: Icon(
                        Icons.add,
                        color: Colors.grey[600],
                        size: 20.0,
                      ),
                    )),
              ],
            ),
          ],
        ),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,

          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Item 1'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Item 2'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
