import 'package:e_commerce_sport_shirts/Cards/GridCard.dart';
import 'package:e_commerce_sport_shirts/accessories/Cards/MyDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';

import 'file:///C:/AndroidStudioProjects/e_commerce_sport_shirts/lib/accessories/Cards/myAppBar.dart';

import '../Cards/HorCard.dart';

void main() => runApp(MaterialApp(
      home: Explorer(),
    ));

class Explorer extends StatefulWidget {
  @override
  _Explorer createState() => _Explorer();
}

class _Explorer extends State<Explorer> {
  final appTitle = 'FIFA World Cup Kits';

  @override
  Widget build(BuildContext context) {
    var usd = Currency.create('USD', 0);
    Money costPrice = Money.fromInt(165, usd);
    String tradeMark = 'NIKE';
    String pieceName = 'BRASIL AUTHENTIC\nJERSEY 2018';
    String imagePath = 'assets/images/01.png';

    bool visible = true;
    List<HorCard> Horcards = [
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      HorCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
    ];

    bool ViewLists = true;
    List<GridCard> Gcards = [
      GridCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      GridCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      GridCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      GridCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      GridCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      GridCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
      GridCard(
        costPrice: costPrice,
        imagePath: imagePath,
        tradeMark: tradeMark,
        pieceName: pieceName,
      ),
    ];

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: this.appTitle,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              children: [
                Text(
                  'Authentic World Cup Kits',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 30.0,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.06,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  color: Colors.grey, // set border color
                  width: 0.5,
                  style: BorderStyle.solid), // set border width
              // set rounded corner radius
            ),
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Search Kits',
                hintStyle: TextStyle(
                  color: Colors.grey,
                ),
                border: InputBorder.none,
                icon: Icon(Icons.search),
              ),
            ),
          ),
          Container(
            // height: MediaQuery.of(context).size.height*0.055,
            // width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.02,
                MediaQuery.of(context).size.width * 0.02,
                0,
                MediaQuery.of(context).size.width * 0.02),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      ViewLists = true;
                      print(ViewLists);
                    });
                  },
                  child: Icon(
                    Icons.view_list,
                    color: Colors.grey[600],
                    size: 30.0,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      ViewLists = false;
                      print(ViewLists);
                    });
                  },
                  child: Icon(
                    Icons.view_module_sharp,
                    color: Colors.grey[600],
                    size: 30.0,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.49, 0, 0, 0),
                  child: Text(
                    'FILTER',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.04, 0, 0, 0),
                  child: Text(
                    'SORT',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: !visible,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.5848,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                    child: ListView.builder(
                            itemCount: Horcards.length,
                            itemBuilder: (context, index) {
                              return new Card(
                                shadowColor: Colors.white,
                                margin: EdgeInsets.all(0.8),
                                elevation: 15.0,
                              child: new InkResponse(
                                    child: Horcards[index],
                                    onTap: (){
                                      Navigator.pushNamed(context, '/ProductDetails');
                                },),
                              );
                            },
                          )
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: visible,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.5848,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                      child:
                      GridView.builder(
                        itemCount: Gcards.length,
                        gridDelegate:
                        new SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 5,
                            crossAxisSpacing: 5,
                            mainAxisExtent:
                            MediaQuery.of(context).size.height *
                                0.38),

                        itemBuilder: (context, index) {
                          return new Card(
                            shadowColor: Colors.white,
                            margin: EdgeInsets.all(0.8),
                            elevation: 15.0,
                            child: new InkResponse(
                              child: Gcards[index],
                              onTap: (){
                                Navigator.pushNamed(context, '/ProductDetails');
                              },),
                          );
                        },
                      ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.08,
            width: MediaQuery.of(context).size.width,
            child: Container(
              color: Colors.black45,
              child: RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.05,
                            MediaQuery.of(context).size.width * 0.05,
                            MediaQuery.of(context).size.width * 0.010,
                            MediaQuery.of(context).size.width * 0.05),
                        child: Icon(
                          Icons.copyright_outlined,
                          size: 20.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    WidgetSpan(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.00001,
                            MediaQuery.of(context).size.width * 0.05,
                            MediaQuery.of(context).size.width * 0.0125,
                            MediaQuery.of(context).size.width * 0.05),
                        child: Text(
                          'FIFA World Cup Kits 2018',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      drawer: MyDrawer(),
    );
  }
}

