import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Steps extends StatelessWidget {
  const Steps({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 40.0,
          height: 40.0,
          decoration: new BoxDecoration(
            color: Colors.orange,
            shape: BoxShape.circle,
          ),
          child: Text(
            '1',
            textAlign: TextAlign.center,
            style: TextStyle(),
          ),
        ),
        SizedBox(
          height: 1.0,
          width: 100.0,
          child: Container(
            color: Colors.grey,
          ),
        )
      ],
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }
}
