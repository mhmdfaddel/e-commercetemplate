import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('E Commerce App'),
          ),

          ListTile(
            title: Text('Home'),
            onTap: () {

              Navigator.pushNamed(context, '/Explorer');
            },
          ),
          ListTile(
            title: Text('Cart'),
            onTap: () {
              Navigator.pushNamed(context, '/CartPage');
            },
          ),
        ],
      ),
    );
  }
}
