import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class myAppBar extends StatefulWidget implements PreferredSizeWidget {
  myAppBar({
    Key key,
    @required this.title,
  })  : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize;

  final String title;

  @override
  _myAppBarState createState() => _myAppBarState();
}

class _myAppBarState extends State<myAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        widget.title,
        style: TextStyle(
          fontSize: 20.0,
          color: Colors.black87,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Colors.grey[600]),
      actions: <Widget>[
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/CartPage');
              },
              child: Icon(
                Icons.shopping_cart_outlined,
                color: Colors.grey[600],
                size: 20.0,
              ),
            )),
      ],
    );
  }
}
